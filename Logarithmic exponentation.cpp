﻿// Logarithmic exponentation.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
using namespace std;

int power(int x, unsigned int y) {
	if (y == 0)
		return 1;
	else if (y % 2 == 0)
		return power(x, y / 2)*power(x, y / 2);
	else
		return x * power(x, y / 2)*power(x, y / 2);
}

int main() {
	int x = 4;
	unsigned int y = 3;

	cout << power(x, y);
}
