﻿// Count inversion.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
using namespace std;
/*int power(int x, unsigned int y) {
	if (y == 0)
		return 1;
	else if (y % 2 == 0)
		return power(x, y / 2)*power(x, y / 2);
	else
		return x * power(x, y / 2)*power(x, y / 2);
}

int main() {
	int x = 4;
	unsigned int y = 3;

	cout << power(x, y);
}*/

//For each element, count number of elements which are on right side of it and are smaller than it.

int getInvCount(int arr[], int n) {
	int inv_count = 0;
	for (int i = 0; i < n - 1; i++)
		for (int j = i + 1; j < n; j++)
			if (arr[i] > arr[j])
				inv_count++;

	return inv_count;
}

int main() {
	int arr[] = { 1, 20, 6, 4, 5 };
	int n = sizeof(arr) / sizeof(arr[0]);
	cout << " Number of inversions are "
		<< getInvCount(arr, n);
	return 0;
}

